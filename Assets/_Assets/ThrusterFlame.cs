﻿using UnityEngine;
using System.Collections;

public class ThrusterFlame : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        float temp = Random.Range(0.3f, 1.0f);
        transform.localScale = new Vector3(0.3760396f, temp, 0.3760396f);
    }
}

﻿using UnityEngine;
using System.Collections;

public class SpaceshipControl : MonoBehaviour {

    public float turnspeed = 5.0f;
    public float speed = 5.0f;
    private float trueSpeed = 0.0f;
    public float strafeSpeed = 5.0f;

    void Update()
    {

        float roll = Input.GetAxis("Roll");
        float pitch = Input.GetAxis("Pitch");
        float yaw = Input.GetAxis("Yaw");
        Vector3 strafe = new Vector3(Input.GetAxis("Horizontal") * strafeSpeed * Time.deltaTime, Input.GetAxis("Vertical") * strafeSpeed * Time.deltaTime, 0);

        float power = Input.GetAxis("Power");

        //Truespeed controls

        if (trueSpeed < 10 && trueSpeed > -3)
        {
            trueSpeed += power;
        }
        if (trueSpeed > 10)
        {
            trueSpeed = 9.99f;
        }
        if (trueSpeed < -3)
        {
            trueSpeed = -2.99f;
        }
        if (Input.GetKey("backspace"))
        {
            trueSpeed = 0;
        }

        GetComponent<Rigidbody>().AddRelativeTorque(pitch * turnspeed * Time.deltaTime, yaw * turnspeed * Time.deltaTime, roll * turnspeed * Time.deltaTime);
        GetComponent<Rigidbody>().AddRelativeForce(0, 0, trueSpeed * speed * Time.deltaTime);
        GetComponent<Rigidbody>().AddRelativeForce(strafe);
    }
}
